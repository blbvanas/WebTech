<?php

include "../dbconnect.php";

$db = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // van https://www.w3schools.com/php/php_mysql_insert.asp

$query = "INSERT INTO User (FirstName, LastName, BirthDate, Email, PasswordHash) VALUES (?, ?, ?, ?, ?)";
$stmt = $db->prepare($query);
$stmt->execute([$_POST["firstname"], $_POST["lastname"], $_POST["birthdate"], $_POST["email"], $_POST["username"]]);

$fetch = $db->prepare("SELECT FirstName FROM User");
$fetch->execute();
$result = $fetch->fetchAll();
print_r($result);

?>