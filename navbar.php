<!DOCTYPE html>
<html>
<head>
    <style>
        
        /* Add a black background color to the top navigation */
        .topnav {
            background-color: #333;
            overflow: hidden;
        }

        /* Style the links inside the navigation bar */
        .topnav a {
            float: left;
            color: #f2f2f2;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
        }

        /* Change the color of links on hover */
        .topnav a:hover {
            background-color: #ddd;
            color: black;
        }

        /* Add a color to the active/current link */
        .topnav a.active {
            background-color: #04AA6D;
            color: white;
        }

        .division {
            border: 1px solid black;
            margin: 100px 0;
        }

    </style>

    <script>
        function CreateButtons(subfora) {
            document.write("<a href='subforum.html?ProductID=12345'>Nimbus 2000</a>");
        }
    </script>
    
    <?php
        include "../dbconnect.php";

        $db = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // van https://www.w3schools.com/php/php_mysql_insert.asp
        $stmt = $db->prepare("SELECT ProductName FROM Product");
        $stmt->execute();
        $products = $stmt->FetchAll();

    ?>
</head>

<!--
Nog niet gelinkt aan de files, denk voorlopig dat we gwn copypasten. Vond het gwn fijn om het op een centrale plek te hebben

Niet vergeten `class="active"` toe te voegen aan de page waar we op zitten.
-->
<nav>
    <div id="navbar" class="topnav">
        <script>
            CreateButtons();
        </script>
        <a style="float:right" href="newquestion.html">New question</a>
        <a style="float:right" href="registratie.html">Account</a>
        <a style="float:right" href="home.html">Home</a> 
      </div>
</nav>
